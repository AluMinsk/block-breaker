﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBlock : MonoBehaviour
{
    public int life;
    public Sprite Crack;
    public Score score;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        
        if (life >= 2)
        {
            score.CountScore(+5);
            Destroy(gameObject);
        }
        else if (life >= 1)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = Crack;
        }

    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //score++;
        // Debug.Log("Boom!");
        life ++;
        //Debug.Log(life);
      
    }

    private void ObjectSprite()
    {
        
    }
   
}
