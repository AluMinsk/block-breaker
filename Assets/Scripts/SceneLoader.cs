﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneLoader : MonoBehaviour
{
    public Score score;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ExitGame();
        }
    }
    public void LoadNextScene()
    {

            Scene activeScene = SceneManager.GetActiveScene();
            int currentScenendex = activeScene.buildIndex;
            SceneManager.LoadScene(currentScenendex + 1);
        
    }

    public void LoadCurrentScene()
    {
        
        Scene activeScene = SceneManager.GetActiveScene();
        int currentScenendex = activeScene.buildIndex;
        SceneManager.LoadScene(currentScenendex);
    }
       


    public void LoadNextSceneDelay()
    {
        Invoke("LoadNextScene",0.3f);

    }

    public void LoadStartScene()
    {
        SceneManager.LoadScene(0);
    }

    public void ExitGame()
    {
        Application.Quit();
    }


   }
