﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    public int score;
    public TextMeshProUGUI scoreText;


    // Start is called before the first frame update
    void Start()
    {
        scoreText.text = "Score: " + score + "";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CountScore(int changeScore)
    {
        score += changeScore;
        scoreText.text = "Score: " + score + "";

        //Block block = FindObjectOfType<Block>(); 
        //score++;
        ////Debug.Log(score);
        //currentScore.text = score.ToString();

    }

}
        