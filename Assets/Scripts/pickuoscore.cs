﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pickuoscore : MonoBehaviour
{

    [SerializeField] int score = 100;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("pad"))
        {
            GameManager gameManager = FindObjectOfType<GameManager>();
            gameManager.AddScore(score);
            Destroy(gameObject);
        }

    }
}
