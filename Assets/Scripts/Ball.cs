﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ball : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] Pad pad;
    Vector3 ballOffSet;
    bool hasStarted = false;
    Rigidbody2D rigidbody;

    

    // Start is called before the first frame update
    public void Start()
    {
        ballOffSet = transform.position - pad.transform.position;
        rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Vector3 ballOffSet = new Vector3(0, 1, 0);
        //transform.position = pad.transform.position + ballOffSet;

        if (!hasStarted)
        {
            LockBallToPad();
            LaunchBall();
        }
      
    }

    private void LockBallToPad()
    {

        transform.position = pad.transform.position + ballOffSet;
    }
    

    private void LaunchBall()
    {
        if (Input.GetMouseButtonDown(0))
        {
           
            //Debug.Log(rigidbody.velocity);    
            
            float randomX = Random.Range(-10.0f, 10.0f);
            Vector2 newSpeed = new Vector2(randomX, 12);
            rigidbody.velocity = newSpeed.normalized * speed;
           

            //rigidbody.AddForce(new Vector2(2, 10));
            hasStarted = true;

        }
    }

    private void OnDrawGizmos()
    {
        if (Application.isPlaying)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, transform.position + (Vector3)rigidbody.velocity);
        }
    }

    void Restart()
    {
        //LockBallToPad();
        hasStarted = false;
    }

    public void RestartDelay()
    {
        Invoke("Restart", 0.4f);
    }
    public void LoadNextScene()
    {
        Scene activeScene = SceneManager.GetActiveScene();
        int currentScenendex = activeScene.buildIndex;


        //int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;


        SceneManager.LoadScene(currentScenendex + 1);
    }



}
