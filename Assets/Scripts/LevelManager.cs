﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelManager : MonoBehaviour
{

    [SerializeField] int blockCount;
    SceneLoader sceneLoader;
  

    // Start is called before the first frame update
    void Start()
    {
        
        sceneLoader = FindObjectOfType<SceneLoader>();
        Time.timeScale = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void AddBlockCount()
    {

        blockCount++;
    }

    public void BlockDestroyed()
    {
        blockCount--;
        if (blockCount <= 0)
        {

            sceneLoader.LoadNextSceneDelay();
            Time.timeScale = 0.3f;
        }
    }
}
