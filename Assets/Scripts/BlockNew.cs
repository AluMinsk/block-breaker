﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockNew : MonoBehaviour
{
    int life;
    int blockLife;

    [SerializeField] int blockScore;
    [SerializeField] Sprite[] sprites;
    [SerializeField] GameManager gameManager;
    [SerializeField] GameObject pickup;

    LevelManager levelManager;
    SpriteRenderer spriteRenderer;




    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        blockLife = sprites.Length;
        gameManager = FindObjectOfType<GameManager>();
        levelManager = FindObjectOfType<LevelManager>();
        levelManager.AddBlockCount();
    }

    // Update is called once per frame
    void Update()
    {

        
       

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
       
        life++;

        //SpriteRenderer.sprite = sprite[health - hits]
        if (life >= blockLife)
        {
            //score.CountScore(blockScore);
            gameManager.AddScore(blockScore);
            Destroy(gameObject);
            
            levelManager.BlockDestroyed();
            Instantiate(pickup);
        }
        else
        {
            if (sprites[life] == null)
            {
                Debug.LogError(gameObject.name  + "404 sprite not found");

            }
            else
            {
                spriteRenderer.sprite = sprites[life];
            }

           
        }

    }


}
