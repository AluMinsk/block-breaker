﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("Config")]
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] TextMeshProUGUI livesLeftText;

    [Header("UI")]
    [SerializeField] GameObject liveSpritePrefab;
    [SerializeField] Transform livesContainer;

    int totalScore = 0;
    public int totalLives = 3;
    [SerializeField] Image tint;


    YouLost youLost;
    SceneLoader sceneLoader;


    private void Awake()
    {
        GameManager[] gameManagers = FindObjectsOfType<GameManager>();

        if (gameManagers.Length > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }



    }
    // Start is called before the first frame update
    void Start()
    {
        tint.gameObject.SetActive(false);
        totalScore.ToString();
        scoreText.text = "Score: " + totalScore + "";
        //scoreText.text = totalScore.ToString();
        totalLives.ToString();
        livesLeftText.text = "" + totalLives + " lives left";
        DontDestroyOnLoad(gameObject);
        sceneLoader = FindObjectOfType<SceneLoader>();
        

        for (int i = 0; i < totalLives; i++ )
        {
            Instantiate(liveSpritePrefab, livesContainer);
        }
        

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (Time.timeScale == 0f)
            {
                Time.timeScale = 1f;
            }
            else 

            {
                Time.timeScale = 0f;
                

            }
            tint.gameObject.SetActive(Time.timeScale == 0f);
        }


    }
    public void AddScore(int scoreToAdd)
    {
        totalScore += scoreToAdd;
        totalScore.ToString();
        scoreText.text = "Score: " + totalScore + "";
    }

   public void ResetScore()
    {
        totalScore = 0;
        totalScore.ToString();
        scoreText.text = "Score: " + totalScore + "";
    }

    public void LostLife()
    {
        totalLives--;
        totalLives.ToString();
        livesLeftText.text = "" + totalLives + " lives left";
        //sceneLoader.LoadNextSceneDelay();

        if (totalLives <= 0)
        {
            Debug.Log("tbI pidor");
            ResetScore();
            Debug.Log("твой скор "+totalScore+"");

            //Invoke("sceneLoader.LoadStartScene", 0.5f);
            sceneLoader.LoadStartScene();
            totalLives = 3;
            Debug.Log("твои жизни " + totalLives + "");
            scoreText.text = "Score: " + totalScore + "";
            livesLeftText.text = "" + totalLives + " lives left";
            Transform firstChild = livesContainer.GetChild(0);
            Destroy(firstChild.gameObject);
            for (int i = 0; i < totalLives; i++)
            {
                Instantiate(liveSpritePrefab, livesContainer);
            }


        }
        else
        {
            Transform firstChild = livesContainer.GetChild(0);
            Destroy(firstChild.gameObject);
        }
    }
}
