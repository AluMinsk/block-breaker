﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pad : MonoBehaviour
{

    [SerializeField] float minX = 1.2f;
    [SerializeField] float maxX = 14.8f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Input.mousePosition);

        Vector3 mousePosInPixels = Input.mousePosition;
        Vector3 mousePosInUnits = Camera.main.ScreenToWorldPoint(mousePosInPixels);


        float newX = mousePosInUnits.x;
        newX = Mathf.Clamp(newX, minX, maxX);
        Vector2 newPadPos = new Vector2(newX, transform.position.y);
        transform.position = newPadPos;





    }
}
