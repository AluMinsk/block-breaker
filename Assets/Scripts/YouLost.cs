﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class YouLost : MonoBehaviour
{

    GameManager gameManager;
    SceneLoader sceneLoader;
    Ball ball;


    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        sceneLoader = FindObjectOfType<SceneLoader>();
        ball = FindObjectOfType<Ball>();
    }
    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    Debug.Log("Enter!");   
    //}

    //private void OnCollisionExit2D(Collision2D collision)
    //{
    //    Debug.Log("Exit!");
    //}

    //private void OnTriggerEnter2D(Collider2D collision)
    //{

    //    Debug.Log("Enter trigger!");
    //}

    private void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.CompareTag("ball"))
        {
            YouLostLife();
        }
        else
        {

            Destroy(collision.gameObject);
        }
    }

    public void YouLostFinal()
    {
        gameManager.ResetScore();
        sceneLoader.LoadCurrentScene();
    }

    private void YouLostLife()
    {
        gameManager.LostLife();
        ball.RestartDelay();
        //ball.Start();
        //sceneLoader.LoadCurrentScene();
        //sceneLoader.LoadStartScene();
    }

}
